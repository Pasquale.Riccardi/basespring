package com.example.base.basePerProgetti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasePerProgettiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasePerProgettiApplication.class, args);
	}

}
